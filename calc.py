#!/usr/bin/python3
"""
----------------------------------------------------------
                   Programa Calculadora:
----------------------------------------------------------
Calcula sumas y restas de dos números enteros.

Modo de uso: ./calc.py {sumar|restar} <a> <b>

Funciona sin especificar argumentos. Si los
argumentos son inválidos, se imprime el mensaje de uso.
Si no se proporcionan argumentos, no se considera error.

        Se den o no argumentos se imprimirá:
                Sumar 1 y 2: 3
                Sumar 3 y 4: 7
                Restar 5 de 6: 1
                Restar 7 de 8: 1
---------------------------------------------------------
"""
#.. Importación de los argumentos de la línea de comandos
from sys import argv

#.. Definición de la función sumar (a + b)
def sumar(a, b):
    return a + b

#.. Definición de la función restar (a - b)
def restar(a, b):
    return a - b

#.. Procesamiento principal
if __name__ == "__main__":
    try:
        a = int(argv[2])
        b = int(argv[3])
        if argv[1] == "sumar":
            print(f"Sumar {a} y {b}: {sumar(a, b)}")
        elif argv[1] == "restar":
            print(f"restar {b} de {a}: {restar(a, b)}")
    except ValueError:
        print("Usage: ./calc.py {sumar|restar} <a> <b>")
    except IndexError:
        if len(argv) != 1:
            print("Usage: ./calc.py {sumar|restar} <a> <b>")
    print(f"Sumar 1 y 2: {sumar(1, 2)}")
    print(f"Sumar 3 y 4: {sumar(3, 4)}")
    print(f"Restar 5 de 6: {restar(6, 5)}")
    print(f"Restar 7 de 8: {restar(8, 7)}")
